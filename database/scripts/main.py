import pandas as pd
import os


def read_data(path: str) -> pd.DataFrame:
    df = pd.read_csv(path, error_bad_lines=False)
    return df.dropna()


def get_columns(df: pd.DataFrame, columns) -> pd.DataFrame:
    return df[columns]


def delete_file_if_exist(name: str) -> None:
    file_name = name + ".csv"
    if os.path.exists(file_name):
        os.remove(file_name)


def save_as_file(df: pd.DataFrame, name: str) -> None:
    delete_file_if_exist(name)
    df.to_csv(name + ".csv", index=False)


def transform_product_data(path: str) -> None:
    df = read_data(path)
    columns = ["productID", "productName"]
    df = get_columns(df, columns)
    save_as_file(df, "transformed_products")


def create_apriori_array(df: pd.DataFrame) -> pd.DataFrame:
    df = df.groupby(['orderID', 'productID'])['quantity'].sum().unstack().reset_index().fillna(0)
    return df.iloc[:, 1:78].astype(bool).astype(int)


def transform_order_details_data(path: str) -> None:
    df = read_data(path)
    columns = ["orderID", "productID", "quantity"]
    df = get_columns(df, columns)
    df = create_apriori_array(df)
    save_as_file(df, "transformed_order_details")


if __name__ == '__main__':
    transform_product_data("products.csv")
    transform_order_details_data("order_details.csv")
