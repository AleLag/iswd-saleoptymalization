# ISWD - SaleOptymalization

# Server part
Create virtual environment
Ex: virtualenv <name>
Activate venv
source name/bin/activate
Install packages by pip
pip install djangorestframework django-filter numpy pandas mlxtend
Run server ( cwd = ~/backend ) 
python manage.py runserver

# GUI part
Install Node.js and NPM on your machine
cwd = ~/frontend/sale-optimalization
npm install
nmp start
