import {Component, h, Listen, State} from '@stencil/core';
import api from "../../../api";

@Component({
  tag: 'sale-component',
  styleUrl: 'sale-component.scss',
  shadow: true,
})
export class SaleComponent {
  @State() selectedProducts: any = {
    list: [],
    generated: []
  };
  @State() productsList:any = [];

  @Listen('updateSelectedProducts')
  updateSelectedProducts(event: CustomEvent) {
    const {productID, productName, action} = event.detail;
    let product = [{productID, productName}];
    if (action == 'ADD') {
      const isExist = this.selectedProducts.list.find(item => item.productID == productID);
      if (!isExist) {
        this.selectedProducts.list = [...this.selectedProducts.list, ...product];
      }
    } else if (action == 'DELETE') {
      this.selectedProducts.list = [...this.selectedProducts.list.filter(item => item.productID !== productID),];
    }

    this.selectedProducts = {...this.selectedProducts};
    this.getGeneratedProductList();
  }

  getGeneratedProductList = () => {
    let selectedIds = [];
    this.selectedProducts.list.map(product => selectedIds.push(product.productID));
    if(selectedIds.length){
      api.getRecomendedProducts(selectedIds).then(res => {
        this.selectedProducts.generated = res.data;
        this.selectedProducts = {...this.selectedProducts};
      })
    }else{
      this.selectedProducts.generated = [];
    }

  };

  getProductsList = async() =>{
    return api.getProducts().then((res) => {
      return res;
    })
  };

  async componentWillRender() {
    let products = await this.getProductsList();
    this.productsList = [...products.data];
  }

  render() {
    return <div>
      <div class={'container'}>
        <h1 class={'sale--title'}>Sale optimization</h1>
        {this.productsList && (
          <product-list
            productsList={this.productsList}
            selectedProductsList={this.selectedProducts.list}
            generatedProductsList={this.selectedProducts.generated}
          />
        )}

      </div>
    </div>;
  }
}
