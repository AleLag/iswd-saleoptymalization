import {Component, h, Prop, Event} from '@stencil/core';
import {EventEmitter} from "@stencil/core/internal";

@Component({
  tag: 'product-item',
  styleUrl: 'product-item.scss',
  // shadow: true,
})

export class ProductItem {
  @Prop() product?: any;
  @Prop() isAdded?: boolean = false;
  @Prop() isGenerated?: boolean = false;

  @Event({
    eventName: 'updateSelectedProducts',
    composed: true,
    cancelable: true
  }) updateSelectedProducts: EventEmitter;


  updateCustomFormDataField(field) {
    const {id, name, action} = field;
    this.updateSelectedProducts.emit({
      productID: id, productName: name, action});
  };

  onAddCartAction = () => {
    this.updateCustomFormDataField({
      id: this.product.productID,
      name: this.product.productName,
      action: this.isAdded ? 'DELETE' : 'ADD'
    })
  };

  render() {
    return (
      <div class={'product--item mt-3 mb-2'}>
        <h4>{this.product.productName}</h4>
        <div class="btn-group btn-group-sm">
          {!this.isGenerated && (
            <button type={'button'}
                    class={this.isAdded ? 'btn btn-danger' : 'btn btn-primary'}
                    onClick={this.onAddCartAction}>
              {!this.isAdded ? 'Dodaj do koszyka' : 'Usuń z koszyka'}
            </button>
          )}
        </div>
      </div>
    )
  }
}
