import {Component, h, Prop} from '@stencil/core';

@Component({
  tag: 'product-list',
  styleUrl: 'product-list.scss',
  // shadow: true,
})

export class ProductList {
  @Prop() selectedProductsList: [];
  @Prop() generatedProductsList: [];
  @Prop() productsList: [];

  render() {
    return (
      <div class={'row'}>

        <div class={'col-md-4'}>
          <h3 class={'header--h3 list--header'}>Lista produktów</h3>
          {this.productsList.map(product => <product-item product={product}/>)}
        </div>

        <div class={'col-md-4'}>
          <h3 class={'header--h3 list--header'}>Dodane produkty</h3>
          {this.selectedProductsList && (
            <div>
              {this.selectedProductsList.map(product => <product-item product={product} isAdded={true}/>)}
            </div>
          )}
        </div>

        <div class={'col-md-4'}>
          <h3 class={'header--h3 list--header'}>Wygenerowane produkty</h3>
          {this.generatedProductsList && (
            <div>
              {this.generatedProductsList.map(product => <product-item product={product} isGenerated={true}/>)}
            </div>
          )}
        </div>
      </div>

    )
  }
}
