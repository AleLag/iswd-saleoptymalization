import axios from 'axios';

const api = {
  API_URL: 'http://127.0.0.1:8000/api',
  AUTHORIZATION_TOKEN: '',


  getProducts: async() => {
    return await axios.get(api.API_URL + '/products/');
  },

  getRecomendedProducts: async (productsList) => {
    let productIdsUrl = productsList.join(',');
    return await axios.get(api.API_URL + `/recommended`, {
      params: {
        productID: productIdsUrl
      }
    });
  },


};

export default api;
