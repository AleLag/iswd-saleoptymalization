// @ts-ignore
import {Config} from '@stencil/core';
import {sass} from '@stencil/sass';
import { postcss } from "@stencil/postcss";
import autoprefixer from "autoprefixer";

export const config: Config = {
  namespace: 'sale-optimalization',
  outputTargets: [
    {
      type: 'dist',
      esmLoaderPath: '../loader',
    },
    {
      type: 'dist-custom-elements-bundle',
    },
    {
      type: 'docs-readme',
    },
    {
      type: 'www',
      serviceWorker: null, // disable service workers
    },
  ],
  // globalStyle: '.src/assets/global/variables.css',
  plugins: [
    sass({
      injectGlobalPaths: [
        // 'src/global/bootstrap/functions.scss',
        // 'src/global/bootstrap/variables.scss',
        // 'src/global/bootstrap/mixins/breakpoints.scss',
        // 'src/global/_mixins.scss'
      ]
    }),
    postcss({
      plugins: [autoprefixer()],
    }),
  ]
};
