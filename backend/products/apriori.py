import mlxtend
from mlxtend.frequent_patterns import apriori
from mlxtend.frequent_patterns import association_rules
import pandas as pd
import os


def our_apriori(id):
    ROOT_DIR = os.path.dirname(os.getcwd())
    transformed_details = os.path.join(ROOT_DIR,
    'database/data/outputs/transformed_order_details.csv')

    df = pd.read_csv(transformed_details)
    df = df.astype(bool)
    frequent_items = apriori(df, min_support=0.0013, use_colnames=True)
    frequent_items['length'] = frequent_items['itemsets'].apply(lambda x: len(x))
    rules = association_rules(frequent_items, metric='lift', min_threshold=1)
    rules.head()
    rules = rules.sort_values(by='lift', ascending=False)

    cols = ['antecedents','consequents']
    rules[cols] = rules[cols].applymap(lambda x: tuple(x))
    inputs = set(str(id))
    suggestions = pd.DataFrame()
    for index, item in rules.iterrows():
        if(set(item["antecedents"]).issubset(inputs)):
                suggestions = suggestions.append(item)
    suggestions = suggestions.sort_values(by='lift', ascending=False)
    items = list()
    count_result = 3
    for index, item in suggestions.iterrows():
        [items.append(each) for each in list(item["consequents"])]
        items = list(set(items).difference(inputs))
        if len(items) > count_result:
            items = items[0:count_result]
            break
        if len(items) == count_result:
            break
    for i in range(len(items)):
        items[i] = int(items[i])
    return items