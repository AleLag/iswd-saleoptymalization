from rest_framework import serializers
from .models import Product

class ProductSerializer(serializers.ModelSerializer):

    def __init__(self, *args, **kwargs):
        many = kwargs.pop('many', True)
        super(ProductSerializer, self).__init__(many=many, *args, **kwargs)
        
    class Meta:
        model = Product
        fields = ('productID', 'productName')

    def get_productName(self, obj):
        productName = ''
        if obj.productName:
            return obj.productName
    def get_productID(self, obj):
        if obj.productID:
            return obj.productID
