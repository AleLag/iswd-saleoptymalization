from django.shortcuts import render
from rest_framework import viewsets
from .serializers import ProductSerializer
from .models import Product
from rest_framework.response import Response
from .apriori import our_apriori
from django.core import serializers



# Create your views here.

class ProductView(viewsets.ModelViewSet):
    serializer_class = ProductSerializer
    

    def get_queryset(self):
        id = self.request.GET.get('productID')
        if id is not None:
            ids = [int(prodID) for prodID in id.split(',')]
            return Product.objects.filter(pk__in=ids)
        else:
            return Product.objects.using('products').all()
    
class RecommendedProductView(ProductView):

    def get_queryset(self):
        id_string = self.request.GET.get('productID')
        ids = [int(id) for id in id_string.split(',')]
        recommended_ids = our_apriori(ids)
        
        return Product.objects.filter(pk__in=recommended_ids)