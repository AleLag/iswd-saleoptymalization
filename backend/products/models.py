from django.db import models

# Create your models here.
class Product(models.Model):
    productID = models.AutoField(primary_key=True)
    productName = models.CharField(max_length=255)
    
    def __str__(self):
        return self.productName
