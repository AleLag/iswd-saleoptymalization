from django.apps import AppConfig
from .sql_converter import sql_converter

class ProductsConfig(AppConfig):
    name = 'products'
    verbose_name = 'Products'
    def ready(self):
        sql_converter()
