import pandas as pd
import sqlite3
import os



def sql_converter():
    ROOT_DIR = os.path.dirname(os.getcwd())
    products_csv = os.path.join(ROOT_DIR,
    'database/data/outputs/transformed_products.csv')
    df = pd.read_csv(products_csv,index_col=False)

    conn = sqlite3.connect(r'SaleOptimalization.db')
    c = conn.cursor()

    c.execute('CREATE TABLE IF NOT EXISTS products_product (\
        productID INTEGER PRIMARY KEY AUTOINCREMENT,\
        productName TEXT)')

    df.to_sql('products_product', conn, if_exists='replace', index=False)
