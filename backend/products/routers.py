class ProductRouter:

    route_app_labels = {'products', 'contenttypes'}

    def db_for_read(self, model, **hints ):
        if model._meta.app_label in self.route_app_labels:
            return 'products'
        return None
    
    def db_for_write(self, model, **hints):
        if model._meta.app_label in self.route_app_labels:
            return 'products'
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        All non-auth models end up in this pool.
        """
        return True